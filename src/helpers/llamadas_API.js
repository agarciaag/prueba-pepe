import axios from 'axios';
import { baseUrl } from '../helpers/config';


export const postTable = data => {
    const TablesURL = `${baseUrl}/tables/`;
    axios
        .post(TablesURL, { id: data.id, size: data.size , name: data.name })
        .then(res => {
            console.log('el post esta hecho', res);
        })
        .catch(error => {
            console.log('error al postear el carrito', error);
        });
};


