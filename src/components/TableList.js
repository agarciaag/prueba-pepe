import React from "react";
import TableCard from "./TableCard";
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";
import Button from '@material-ui/core/Button';
import {postTable} from '../helpers/llamadas_API';
import TextField from '@material-ui/core/TextField';



const TableList = (props) => {
    console.log("estas son las mesas tableList:",props);
    const [id, setId] = React.useState('16');
    const handleChange = (event) => {
        setId(event.target.value);
    };
    const tableData = {
        id: id,
        name: `mesa ${id}`,
        size: "6"
    };
    if (!props.tables) {
        return(
        <p>No hay mesas</p>
    )}
    else {
        return(
          <>
            <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="center"
                className="App"
                spacing={2}
            >
                    {props.tables.map((item, index) => {
                        return (
                            <TableCard tableName={item.name} tableId={item.id} tableSize={item.size}/>
                        );
                    })}
                </Grid>
              <Grid
                  container
                  direction="row"
                  justify="center"
                  alignItems="center"
                  className="App"
                  spacing={5}
              >
                      <Grid item>
                          <TextField
                              id="outlined-name"
                              label="Id"
                              value={id}
                              onChange={handleChange}
                              variant="outlined"
                          />
                      </Grid>
                     <Grid item>
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={() =>
                                postTable(tableData)
                            }
                        >
                            Añadir nueva mesa
                        </Button>
                  </Grid>

              </Grid>
          </>
    )}
};

TableList.propsTypes = {
   tables:  PropTypes.array,
};

export default TableList;