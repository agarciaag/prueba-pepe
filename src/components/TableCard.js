import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";
import {Link} from "react-router-dom";
import deleteTable from '../helpers/llamadas_API'


const useStyles = makeStyles({
    card: {
        maxWidth: 280,
        padding : 10

    },
    media: {
        width: "100%",

    },
    mediaContainer: {
        maxWidth: 250,
    }
});


const TableCard = (props ) => {
    const classes = useStyles();
    const url = "/menu/"+props.tableId;
    return(

        <Grid item>
             <Card className={classes.card}>
                <CardActionArea component={Link} to={url}>
                    <Grid className={classes.mediaContainer}>
                        <img src={"http://geogebra.es/cvg_primaria/banco/iconos/mesa_redonda_con_sillas.png"} className={classes.media} title={"table"}  alt={"imagen de mesa"}/>
                    </Grid>
                    <CardContent>
                        <Grid container
                              direction="column"
                              justify="center"
                              alignItems="center">
                            <Grid item>
                                <Typography gutterBottom variant="h5" component="h1">
                                    {props.tableName}
                                </Typography>
                                <Typography gutterBottom variant="h6" component="h2">
                                    {props.tableSize} personas
                                </Typography>
                            </Grid>
                        </Grid>
                    </CardContent>
                </CardActionArea>
            </Card>
        </Grid>
    )
};

TableCard.propTypes = {
    tableName: PropTypes.string,
    tableSize: PropTypes.string,
    tableId: PropTypes.string
};

export default TableCard;
