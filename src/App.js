import React, { Component } from 'react';
import './App.css';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import TableList from "./components/TableList";
import {baseUrl} from "./helpers/config";
import axios from "axios";



class App extends Component {

    state = {
        tables: [{
            id:21,
            name:'mesa por defecto',
            size: 7
        }],
    };


    componentDidMount(){
            const TablesURL = `${baseUrl}/tables`;
            axios.get(TablesURL)
                .then(res => {
                    let tables = res.data; //Taking just the required data
                    if (typeof tables === 'object') {
                        console.log("recuperacion de mesas", tables);
                        this.setState({ tables });
                    }
                })
                .catch(error => {
                    console.log('error al recuperar las mesas', error);
                });
    }

    render() {
        return (
            <Router>
                <div>
                    <Switch>
                        <Route path="/">
                            <TableList tables={this.state.tables}/>
                        </Route>
                    </Switch>
                </div>
            </Router>
        );
    }
}
export default App;